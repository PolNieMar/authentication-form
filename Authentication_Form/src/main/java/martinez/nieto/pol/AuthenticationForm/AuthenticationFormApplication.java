package martinez.nieto.pol.AuthenticationForm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthenticationFormApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthenticationFormApplication.class, args);
	}

}
