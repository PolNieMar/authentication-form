package martinez.nieto.pol.AuthenticationForm;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class ControllerWeb {

    private static final String CORRECT_PASSWORD = "123456";
    private static final int MAX_ATTEMPTS = 3;

    private final Map<String, Integer> attemptsMap = new HashMap<>();
    private final List<String> registeredUsers = new ArrayList<>();

    @GetMapping("/login")
    public String showLoginForm(Model model) {
        model.addAttribute("attemptsLeft", MAX_ATTEMPTS);
        return "loginForm";
    }

    @PostMapping("/login")
    public String login(@RequestParam("username") String username,
            @RequestParam("password") String password,
            Model model) {
        if (username.isEmpty() || password.isEmpty()) {
            model.addAttribute("attemptsLeft", MAX_ATTEMPTS);
            model.addAttribute("errorMessage", "Username and password cannot be empty.");
            return "loginForm";
        }

        if (registeredUsers.contains(username)) {
            int attemptsLeft = attemptsMap.getOrDefault(username, MAX_ATTEMPTS);
            if (password.equals(CORRECT_PASSWORD)) {
                return "redirect:/welcome?username=" + username;
            } else {
                attemptsLeft--;
                attemptsMap.put(username, attemptsLeft);
                if (attemptsLeft > 0) {
                    model.addAttribute("attemptsLeft", attemptsLeft);
                    model.addAttribute("errorMessage", "Incorrect password. " + attemptsLeft + " attempts left.");
                    return "loginForm";
                } else {
                    return "redirect:/blocked";
                }
            }
        } else {
            registeredUsers.add(username);
            attemptsMap.put(username, MAX_ATTEMPTS);
            return "redirect:/login";
        }
    }

    @GetMapping("/welcome")
    public String showWelcomePage(@RequestParam("username") String username, Model model) {
        model.addAttribute("username", username);
        return "welcomePage";
    }

    @GetMapping("/blocked")
    public String showBlockedPage() {
        return "blockedPage";
    }
}